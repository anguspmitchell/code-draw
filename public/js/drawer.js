var tool = null;
var path = null, convexHull = null, dataStructure = null;
var stage = 1;
var setCount = 0;
var arrayCount = 0;
var shapes = [];
var shapes_stage1 = [];
var markerColor = "";

$(document).ready(function(){
    openModal("Lesson 1: Arrays", "Draw a few shapes on the canvas!");
 	markerColor = Math.random() > 0.5 ? '#FF5050' : '#3333ff';
 	var canvas = document.getElementById('main-canvas');
	paper.setup(canvas);
	tool = new paper.Tool();
	tool.minDistance = 10;
	
	tool.onMouseDown = function(event) {
        if(convexHull != null){
            convexHull.remove();
        }

        path = new paper.Path();
        path.strokeWidth = 5;
        path.strokeColor = markerColor;

        path.add(event.point);
    }

    tool.onMouseDrag = function(event){
        path.add(event.point);
    }  
    
    tool.onMouseUp = function(event){
        var shape = determineShape(path);
        //shapes.push(shape);
    
        //IF master shape DNE
            // IF circle/rectangle, make master shape
        //Else master shape exists
            // IF subsidiary shape (line in array or circle/rect in set) THEN update master shape
            // Else not subsidiary shape
                // If line or other no master shape
                // If circle or rectangle new master shape
        if(stage == 1){
            $("#code-comment-shapes").append('<span class="commentedOut"><br>// "' + shape.name + '"</span>');
            shapes_stage1.push(shape);
        }
        else if(stage == 2){    
            if(dataStructure == null){
                if(shape.name == "Circle" || shape.name == "Rectangle"){
                    createNewDataStructure(shape);
                }
            }
            else{ // if master shape exists handle it in this method
                if(!checkSubsidiaryShape(shape, dataStructure)){
                // Else not subsidiary shape
                    // If line or other no master shape
                    // If circle or rectangle new master shape		        
                    if(shape.name == "Circle" || shape.name == "Rectangle"){
                        createNewDataStructure(shape);
                    }
                    else{
                        dataStructure = null;
                    }
                }
            }
        }
    }      
});

function openModal(title, text, btnText){
	$("#dialog-modal .modal-body").html(text);
	$("#dialog-modal .modal-title").html(title);
	if(btnText != null){
		$("#dialog-modal .btn-primary").html(btnText);
	}
	$("#dialog-modal").modal("show");
}


$(document).on("click", "#btn-done-drawing", function(){
	if(stage == 1){
	    if(shapes_stage1.length == 0){
	        openModal("Lesson 1: Arrays", "Start out by drawing some shapes! Hit the green button when you're done.");
	    }
	    else if(shapes_stage1.length == 1){
	        openModal("Lesson 1: Arrays", "Try drawing more than one shape!");
	    }
	    else{
            // Popup
            openModal("Lesson 1: Arrays", "We want to store these shape names in memory so that we can play with them in the next lesson. Try drawing a long box that is divided into " + shapes_stage1.length + " compartments. That’s where we’ll store the shape names.");
			//$("#myModal").modal("show");
            // Clear the canvas
            var shapePath;
            for(var i = 0; i < shapes_stage1.length; i++){
                shapePath = shapes_stage1[i].path;
                shapePath.remove();
            }
            //shapes_stage1 = [];
            if(convexHull != null){
                convexHull.remove();
            }
            stage = 2;
        }
	}
	else if(stage == 2){
		if(dataStructure != null){
			if(dataStructure.name == "Array"){
				if(dataStructure.count == shapes_stage1.length){
					var arrayNumber = arrayCount++;
					var codeString = "<span class='commentedOut'><br><br>// Method 1</span>"
					codeString += "<br><span class='keyword'>String</span>[] shapeArray = <span class='keyword'>new</span> <span class='keyword'>String</span>[" + dataStructure.count + "];";
					for(var i = 0; i < shapes_stage1.length; i++){
						codeString +=  '<br>shapeArray[' + i + '] = <span class="string">"' + shapes_stage1[i].name + '"</span>;';
					}
					codeString += "<span class='commentedOut'><br><br>// Method 2</span>";
					codeString += "<br><span class='keyword'>String</span>[] shapeArray = {";
					for(var i = 0; i < shapes_stage1.length; i++){
						if(i != 0){
							codeString += ", ";
						}
						codeString +=  '<span class="string">"' + shapes_stage1[i].name + '"</span>';
					}
					codeString += "};"
					$("#code-array-initialization").html(codeString);
					openModal("Lesson 1: Arrays", "Nice work! You just drew an array. This data structure is the foundation of many other data structures we will study. For now you can think of it as a list of common elements with a fixed length.", "Next lesson");
				    shapes_stage1 = [];
				    stage = 3;
				}
				else{
				    console.log(dataStructure.count + " / " + shapes_stage1.length);
					openModal("Lesson 1: Arrays", "Nice job! You drew an array BUT with the wrong number of compartments. Draw as many comparments as shapes that you just drew.");
				}
			}
			else{
				openModal("Lesson 1: Arrays", "You did draw a data data structure, but not an array. Sorry! Try again.");
			}
			// else if(dataStructure.name == "Set"){
				// var setNumber = setCount++;
				// if(dataStructure.count == 0){
					// $("#text-area-code").append("\nSet&lt;Integer&gt; set" + setNumber + " = new HashSet&lt;Integer&gt;()");
				// }
			// }
		}
		else{
			openModal("Lesson 1: Arrays", "Sorry! Don't see a data structure here!");
		}		
	}
});

$(document).on("click", "#dialog-modal .btn-primary", function(){
	$("#dialog-modal").modal("hide");
	if(stage == 3){
		alert("It's a short demo! Thanks for checking it out.");
	}
});