function determineShape(shapePath){
	var shape = {};	
	
    var firstPoint = shapePath.firstSegment.point;
    var lastPoint = shapePath.lastSegment.point;
	var p0_x = firstPoint.x;
	var p0_y = firstPoint.y;
	var lastSegment = shapePath.lastSegment;
	var pn_x = lastPoint.x;
	var pn_y = lastPoint.y;
	
	var dist_start_finish = dist(firstPoint, lastPoint);
	var length_path = shapePath.length;
	var area_path_paperCalc = shapePath.area;
    convexHull = calcConvexHull(shapePath);
	//angleTest();
	var perimeter_convexHull = convexHull.length;
	var area_convexHull = Math.abs(convexHull.area);
	
	// Perim CH ^ 2 / Area CH
	var thinnessRatio = Math.pow(perimeter_convexHull, 2) / area_convexHull;
	// Smallest rectangle that encloses shape
	var top = {}, bottom = {}, left = {}, right = {};
	top.value = null, bottom.value = null, left.value = null, right.value = null;
	calcEnclosingRectangle(path, top, bottom, left, right);
	var perimeter_enclosingRectangle = calcPerimeterEnclosingRectangle(top, bottom, left, right);
	var ratioSides_enclosingRectangle = calcRatioSidesEnclosingRectangle(top, bottom, left, right);
	var enclosingRectangleRatio = perimeter_convexHull / perimeter_enclosingRectangle;
	var area_innerTriangle = calcMaxInnerTriangleArea(convexHull);
	var triangleConvexHullRatio = area_innerTriangle / area_convexHull;
	
/* 	console.log("(" + p0_x + ", " + p0_y + ") (" + pn_x + ", " + pn_y + ")");
	console.log("Dist: " + dist_start_finish);
	console.log("Length: " + length_path);
	console.log("Length pi r2: " + (Math.pow(length_path, 2) / 4 / Math.PI));
	console.log("Ppr Area: " + area_path_paperCalc);
	console.log("CH Perim: " + perimeter_convexHull);
	console.log("CH Area: " + area_convexHull);
	console.log("Thinness ratio: " + thinnessRatio);
	console.log("Encl Rect Ratio: " + enclosingRectangleRatio); */
	console.log("Triangle/CH ratio: " + triangleConvexHullRatio);
	console.log("Ratio sides rect: " + ratioSides_enclosingRectangle);
	
	shape.path = shapePath;//.clone();
	shape.orientation = null;
	if(thinnessRatio > 90){
		//$("#text-area-code").append("\nline");
		shape.name = "Line";	
		var slope = calcSlope_canvasCoords(firstPoint, lastPoint);
		console.log("slope: " + slope);
		if(slope < 0.2 && slope > -0.2){
		    orientation = "horizontal";
		}
		else if(slope > 3 || slope < -3){
		    orientation = "vertical";
		}
	}
	else if(thinnessRatio < 12.2){
	    //$("#text-area-code").append("\ncircle");
	    //templateShape = shapePath;
	    shape.name = "Circle";
	}
	else{
		if(triangleConvexHullRatio > 0.65){
			shape.name = "Triangle";
		}
	    else if(enclosingRectangleRatio > 0.9){
	        //$("#text-area-code").append("\nrectangle");
	        //templateShape = shapePath;
			if(ratioSides_enclosingRectangle > 0.88 && ratioSides_enclosingRectangle < 1.15){
				shape.name = "Square";
			}
			else{
				shape.name = "Rectangle";
			}
	    }
	    else{
		    //$("#text-area-code").append("\nothershape");
		    shape.name = "Other";
		}
	}
	
	return shape;
}

function calcMaxInnerTriangleArea(convexHullPath){
	var segments = convexHullPath.segments;
	var n = segments.length
	if(n < 3){
		return 0;
	}
	
	var a_start = 0;
	var b_start = 1;
	var c_start = 2;
	var a = segments[a_start].point;
	var b = segments[b_start].point;
	var c = segments[c_start].point;
	a_max = a;
	b_max = b;
	c_max = c;
	
	var maxArea = 0;
	var currArea = 0;
	
	for(var i = 0; i < n; i++){
		for(var j = 0; j < n; j++){
			for(var k = 0; k < n; k++){
				a = segments[(a_start + i) % n].point;
				b = segments[(b_start + j) % n].point;
				c = segments[(c_start + k) % n].point;
				currArea = calcAreaTriangle(a, b, c);
				if(currArea > maxArea){
					maxArea = currArea;
					a_max = a;
					b_max = b;
					c_max = c;
				}
			}
		}
	}
	
	return maxArea;
}
function calcAreaTriangle(a, b, c){
	return 0.5 * (a.x*b.y + b.x*c.y + c.x*a.y - a.x*c.y - c.x*b.y - b.x*a.y);
}

function createNewDataStructure(shape){
    dataStructure = {};
    dataStructure.lineOrientation = null;
    dataStructure.shape = shape;
    dataStructure.count = 0;
    if(shape.name == "Circle"){
        dataStructure.name = "Set";
    }
    else if(shape.name == "Rectangle"){
        dataStructure.name = "Array";
        dataStructure.count++;
    }
}

function checkSubsidiaryShape(shape, dataStructure){
    if(shape.name == "Line" && dataStructure.name == "Array"){
        // If line orientation is consistent with previous cuts
        // AND line is generally within the array
        // then add to data structure count
        
        if(isLineWithinRectangle(shape.path, dataStructure.shape.path)){
            var matchesOrientation = false;
            if(dataStructure.lineOrientation == null){
                matchesOrientation = true;
                dataStructure.lineOrientation = shape.orientation;
            }
            else if(dataStructure.lineOrientation == shape.orientation){
                matchesOrientation = true;
            }
            
            if(matchesOrientation){
                dataStructure.count++;
            }   
            
            // Return true even if orientation doesn't match
            // Only return false if line is outside rectangle
            return true;
        }
    }
    else if((shape.name == "Circle" || shape.name == "Rectangle") && dataStructure.shape.name == "Set"){
        // If shape is generally within the data structure shape
        // then add to data structure count
        if(isShapeWithinCircle(shape.path, dataStructure.shape.path)){
            dataStructure.count++;
            return true;
        }
    }
    
    return false;
}

function isShapeWithinCircle(shape, circle){
    return false;
}

function isLineWithinRectangle(line, rectangle){
    // Line info
    var endpoint1 = line.firstSegment.point;
    var endpoint2 = line.lastSegment.point;
    var length = dist(endpoint1, endpoint2);
    //var slope = calcSlope_canvasCoords(endpoint1, endpoint2);
    
    // Rect info
    var topWrapper = {}, bottomWrapper = {}, leftWrapper = {}, rightWrapper = {};
    topWrapper.value = null;
    bottomWrapper.value = null;
    leftWrapper.value = null;
    rightWrapper.value = null;
    calcEnclosingRectangle(rectangle, topWrapper, bottomWrapper, leftWrapper, rightWrapper);
    var top = topWrapper.value;
    var bottom = bottomWrapper.value;
    var left = leftWrapper.value;
    var right = rightWrapper.value;
        
    var lengthInRectangle;
    // Both endpoints in rectangle
    if((endpoint1.x <= right && endpoint1.x >= left && endpoint1.y >= top && endpoint1.y <= bottom)
        && (endpoint2.x <= right && endpoint2.x >= left && endpoint2.y >= top && endpoint2.y <= bottom)){
        lengthInRectangle = length;
    }
    // Endpoint 1 in, endoint 2 not
    else if(endpoint1.x <= right && endpoint1.x >= left && endpoint1.y >= top && endpoint1.y <= bottom){
        var intersectPointWrapper = {};
        intersectPointWrapper.point = null;
        findRectangleIntersection(endpoint1, endpoint2, top, bottom, left, right, 
                                                            intersectPointWrapper, {});
        if(intersectPointWrapper.point != null){
            lengthInRectangle = dist(endpoint1, intersectPointWrapper.point);
        }
    }
    // Endpoint 2 in, endpoint 1 not
    else if(endpoint2.x <= right && endpoint2.x >= left && endpoint2.y >= top && endpoint2.y <= bottom){
        var intersectPointWrapper = {};
        intersectPointWrapper.point = null;
        findRectangleIntersection(endpoint1, endpoint2, top, bottom, left, right, 
                                                            intersectPointWrapper, {});
        if(intersectPointWrapper.point != null){        
            lengthInRectangle = dist(intersectPointWrapper.point, endpoint2);  
        } 
    }
    else{ // Neither endpoint in rectangle
        var intersectPointWrapper1 = {};
        var intersectPointWrapper2 = {};  
        intersectPointWrapper1.point = null;
        intersectPointWrapper2.point = null;
        findRectangleIntersection(endpoint1, endpoint2, top, bottom, left, right, 
                                    intersectPointWrapper1, intersectPointWrapper2);
        if(intersectPointWrapper1.point != null && intersectPointWrapper2.point != null){
            lengthInRectangle = dist(intersectPointWrapper1.point, intersectPointWrapper2.point); 
        }
    }

    console.log("l(rect): " + lengthInRectangle + " / l(total): " + length);
    return lengthInRectangle / length > 0.75;
}

function findRectangleIntersection(endpoint1, endpoint2, top, bottom, left, right, 
                                    intersectPoint1, intersectPoint2){
    var slope = calcSlope(endpoint1, endpoint2);  
    var intercept = endpoint1.y - slope * endpoint1.x;
      
    // Check left side
    if((endpoint1.x >= left && endpoint2.x <= left) || (endpoint1.x <= left && endpoint2.x >= left)){
        var intersection_y = slope * left + intercept;
        if(intersection_y >= top && intersection_y <= bottom){
            if(intersectPoint1.point == null){
                intersectPoint1.point =  new paper.Point(left, intersection_y);
            }
            else{
                intersectPoint2.point =  new paper.Point(left, intersection_y);
            }
        }
    }
    // Check right side
    if((endpoint1.x >= right && endpoint2.x <= right) || (endpoint1.x <= right && endpoint2.x >= right)){
        var intersection_y = slope * right + intercept; 
        if(intersection_y >= top && intersection_y <= bottom){
            if(intersectPoint1.point == null){
                intersectPoint1.point = new paper.Point(right, intersection_y);
            }
            else{
                intersectPoint2.point = new paper.Point(right, intersection_y);
            }
        }           
    }    
    // Check top
    if((endpoint1.y >= top && endpoint2.y <= top) || (endpoint1.y <= top && endpoint2.y >= top)){
        if(isInfinity(slope)){
            if(intersectPoint1.point == null){
                intersectPoint1.point = new paper.Point(endpoint1.x , top);
            }
            else{
                intersectPoint2.point = new paper.Point(endpoint1.x , top);
            }
        }
        else{
            var intersection_x = (top - intercept) / slope;
            if(intersection_x >= left && intersection_x <= right){
                if(intersectPoint1.point == null){
                    intersectPoint1.point = new paper.Point(intersection_x, top);
                }
                else{
                    intersectPoint2.point = new paper.Point(intersection_x, top);
                }
            }
        }
    }
    // Check bottom
    if((endpoint1.y >= bottom && endpoint2.y <= bottom) || (endpoint1.y <= bottom && endpoint2.y >= bottom)){
        if(isInfinity(slope)){
            if(intesectPoint1.point == null){
                intersectPoint1.point = new paper.Point(endpoint1.x , top);
            }
            else{
                intersectPoint2.point = new paper.Point(endpoint1.x , top);
            }
        }
        else{
            var intersection_x = (bottom - intercept) / slope;
            if(intersection_x >= left && intersection_x <= right){
                if(intersectPoint1.point == null){
                    intersectPoint1.point = new paper.Point(intersection_x, bottom);
                }
                else{
                    intersectPoint2.point = new paper.Point(intersection_x, bottom);
                }
            }  
        }  
    }   
}

function isInfinity(n){
    return n == Number.POSITIVE_INFINITY || n == Number.NEGATIVE_INFINITY;
}

function calcSlope_canvasCoords(point1, point2){
    // Reverse y coords because canvas y starts at 0 at top
    // and gets bigger going down the page
    return (point2.y - point1.y) / (point1.x - point2.x);
}

function calcSlope(point1, point2){
    // "True" slope
    return (point1.y - point2.y) / (point1.x - point2.x);
}

function calcEnclosingRectangle(shapePath, top, bottom, left, right){
    var point;
    var segments = shapePath.segments;
    for(var i = 0; i < segments.length; i++){
        point = segments[i].point;
        if(i == 0){
            top.value = point.y;
            bottom.value = point.y;
            left.value = point.x;
            right.value = point.x;
        }
        else{
            // y gets bigger going down page
            if(point.y < top.value){
                top.value = point.y;
            }
            if(point.y > bottom.value){
                bottom.value = point.y;
            }
            if(point.x < left.value){
                left.value = point.x;
            }
            if(point.x > right.value){
                right.value = point.x;
            }
        }
    }
}
function calcPerimeterEnclosingRectangle(top, bottom, left, right){
    return 2 * (right.value - left.value) + 2 * (bottom.value - top.value); // bottom is a bigger num than top
}

function calcRatioSidesEnclosingRectangle(top, bottom, left, right){
	return (right.value - left.value) / (bottom.value - top.value); // bottom is a bigger num than top
}

// function computeConvexHullPath(path){
//     var segments = path.segments;
//     if(segments.length < 3){
//         return;
//     }
//     
//     var convexHull = new paper.Path();
//     convexHull.strokeColor = "#FF0000";
//     var startingPoint = findLeftMostPoint(path);
//     var pointOnHull = startingPoint;
// 
//     var endpoint = null, potentialEndpoint;
//     
//     var counter = 0;
//     do {
//         var newSegment = new paper.Segment(pointOnHull);
//         convexHull.add(newSegment);      
//         
//         endpoint = null;
//         
//         // Draw a line to all potential endpoints
//         for(var i = 0; i < segments.length; i++){
//             potentialEndpoint = segments[i].point; 
//           
//             // If end point hasn't been initialized then 
//             // initialize it and skip the rest
//             if(endpoint == null){
//                 endpoint = potentialEndpoint;
//             }
//             else{
//                 if(orientation(pointOnHull, potentialEndpoint, endpoint) == 2){
//                     endpoint = potentialEndpoint;
//                 }
//             }
//         }
// 
//         pointOnHull = endpoint;
//         counter++;
//     } while(endpoint != startingPoint && counter < 10000);
//     
//     
//     console.log(counter);
//     return convexHull;
// }
// 
// function orientation(p, q, r)
// {
//     var val = (q.y - p.y) * (r.x - q.x) -
//               (q.x - p.x) * (r.y - q.y);
//  
//     if (val == 0) return 0;  // colinear
//     return (val > 0)? 1: 2; // clock or counterclock wise
// }

function findLeftMostPoint(path){
    var leftMostPoint = null;
    var point;
    var segments = path.segments;
    
    for(var i = 0; i < segments.length; i++){
        point = segments[i].point;
        if(leftMostPoint == null){
            leftMostPoint = point;
        }

        if(point.x < leftMostPoint.x){
            leftMostPoint = point;
        }
    }
    
    return leftMostPoint;
}

function calcConvexHull(path){
    var segments = path.segments;
    if(segments.length < 3){
        return path;
    }
    
    var convexHull = new paper.Path();
    convexHull.visible = false;
    //convexHull.strokeColor = "#FF0000";
    
    // Start at left most point
    var startingPoint = findLeftMostPoint(path);
    convexHull.add(startingPoint);
    var currPoint, prevPoint, nextPoint, tempPoint;
    currPoint = startingPoint;
    prevPoint = new paper.Point(startingPoint.x, startingPoint.y + 10);
    
    var counter = 0;
    while(nextPoint != startingPoint && counter < 10000){
        // Loop through all points and set next point equal to the one
        // with the smallest angle between it and the current point
        var smallestAngle = 2 * Math.PI;
        var maxDistance = 0;
        for(var i = 0; i < segments.length; i++){
            var angle = angle_clockwise_canvasCoords(prevPoint, currPoint, segments[i].point);
            var distance = dist(currPoint, segments[i].point);
            //console.log(angle);
            if(!isNaN(angle)){// && angle <= smallestAngle){
            
                //console.log(angle + " / " + distance + " / " + maxDistance)
                if(angle < smallestAngle || (distance > maxDistance && angle == smallestAngle)){
                    nextPoint = segments[i].point;
                        //    console.log(smallestAngle + " (" + prevPoint.x + ", " + prevPoint.y + ") (" + currPoint.x + ", " + currPoint.y + ") (" + nextPoint.x + ", " + nextPoint.y + ")");

                    smallestAngle = angle;
                    maxDistance = distance;
                }
            }
        }
        //console.log(smallestAngle + " (" + prevPoint.x + ", " + prevPoint.y + ") (" + currPoint.x + ", " + currPoint.y + ") (" + nextPoint.x + ", " + nextPoint.y + ")");
        convexHull.add(nextPoint);
        tempPoint = currPoint;
        currPoint = nextPoint;
        prevPoint = tempPoint;
        counter++;
    }
    
    return convexHull
}

function angleTest(){
    var p_current = new paper.Point(0, 0);
//     var p_previous = new paper.Point(0, -1);
//     var p_130 = new paper.Point(Math.sqrt(2), Math.sqrt(2));
//     var p_3 = new paper.Point(1, 0);
//     var p_430 = new paper.Point(Math.sqrt(2), -Math.sqrt(2));
//     var p_6 = new paper.Point(0, -1);
//     var p_730 = new paper.Point(-Math.sqrt(2), -Math.sqrt(2));
//     var p_9 = new paper.Point(-1, 0);
//     var p_1030 = new paper.Point(-Math.sqrt(2), Math.sqrt(2));
//     var p_12 = new paper.Point(0, 1);
    var p_previous = new paper.Point(0, 1);
    var p_130 = new paper.Point(Math.sqrt(2), -Math.sqrt(2));
    var p_3 = new paper.Point(1, 0);
    var p_430 = new paper.Point(Math.sqrt(2), Math.sqrt(2));
    var p_6 = new paper.Point(0, 1);
    var p_730 = new paper.Point(-Math.sqrt(2), Math.sqrt(2));
    var p_9 = new paper.Point(-1, 0);
    var p_1030 = new paper.Point(-Math.sqrt(2), -Math.sqrt(2));
    var p_12 = new paper.Point(0, -1);    

    console.log("(" + p_730.x + ", " + p_730.y + ") " + angle_clockwise(p_previous, p_current, p_730));
    console.log("(" + p_9.x + ", " + p_9.y + ") " + angle_clockwise(p_previous, p_current, p_9));
    console.log("(" + p_1030.x + ", " + p_1030.y + ") " + angle_clockwise(p_previous, p_current, p_1030));
    console.log("(" + p_12.x + ", " + p_12.y + ") " + angle_clockwise(p_previous, p_current, p_12));    
    console.log("(" + p_130.x + ", " + p_130.y + ") " + angle_clockwise(p_previous, p_current, p_130));
    console.log("(" + p_3.x + ", " + p_3.y + ") " + angle_clockwise(p_previous, p_current, p_3));
    console.log("(" + p_430.x + ", " + p_430.y + ") " + angle_clockwise(p_previous, p_current, p_430));
    console.log("(" + p_6.x + ", " + p_6.y + ") " + angle_clockwise(p_previous, p_current, p_6));
}

function calcAngle_clockwise(prevPoint, currPoint, nextPoint){
//     var a_length = dist(prevPoint, currPoint);
//     var b_length = dist(currPoint, nextPoint);
    var v1_x = (currPoint.x - prevPoint.x);
    var v1_y = (currPoint.y - prevPoint.y);
    var v2_x = (currPoint.x - nextPoint.x);
    var v2_y = (currPoint.y - nextPoint.y);
    var dotproduct = v1_x * v2_x + v1_y * v2_y;
//     return Math.acos(dotproduct / (a_length * b_length));
    //var determinant = (currPoint.x - prevPoint.x)*(currPoint.y - nextPoint.y) - (currPoint.y - prevPoint.y)*(currPoint.x - nextPoint.x);
    var determinant = v1_x * v2_y - v1_y * v2_x;
    return Math.atan2(determinant, dotproduct);
}

function innerAngle(prevPoint, currPoint, nextPoint){
    var v1_length = dist(prevPoint, currPoint);
    var v2_length = dist(currPoint, nextPoint);
    var v1_x = (currPoint.x - prevPoint.x);
    var v1_y = (currPoint.y - prevPoint.y);
    var v2_x = (currPoint.x - nextPoint.x);
    var v2_y = (currPoint.y - nextPoint.y);
    var dotproduct = v1_x * v2_x + v1_y * v2_y;
    var cosx = dotproduct / (v1_length * v2_length);
    return Math.acos(cosx);    
}

function angle_clockwise_canvasCoords(prevPoint, currPoint, nextPoint){
    // canvas coords meaning that y gets bigger as you go down the page
    var inner = innerAngle(prevPoint, currPoint, nextPoint);
    var v1_x = (currPoint.x - prevPoint.x);
    var v1_y = (currPoint.y - prevPoint.y);
    var v2_x = (currPoint.x - nextPoint.x);
    var v2_y = (currPoint.y - nextPoint.y);    
    var determinant = v1_x * v2_y - v1_y * v2_x;    
    if(determinant > 0){
        return inner;
    }
    else{
        return 2 * Math.PI - inner;
    }
}

function dist(a, b){
    return Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2));
}
