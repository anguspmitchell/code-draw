# Code Draw Concept #

Short demo at: http://code-draw.herokuapp.com/

Concept was to create an environment for learning the basics of programming in which users free hand draw data structures/control flow and watch as their drawings are transformed into the corresponding code. Algorithm for shape identification is mostly based on this paper - http://web.ist.utl.pt/mjf/publications/2004-1999/pdf/grec99.pdf

Demo: Drawn Shapes --> Text
![Shapes](screenshots/shapes.png)

Demo: Drawn Array --> Code
![Array](screenshots/array.png)

Uses Paper.js